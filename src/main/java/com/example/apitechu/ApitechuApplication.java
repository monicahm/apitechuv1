package com.example.apitechu;

import com.example.apitechu.models.ProductModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;


@SpringBootApplication
@RestController
public class ApitechuApplication {

	public static ArrayList<ProductModel> productModels;

	public static void main(String[] args) {

		SpringApplication.run(ApitechuApplication.class, args);
		ApitechuApplication.productModels = ApitechuApplication.getTestData();

	}

	private static ArrayList<ProductModel> getTestData() {
		ArrayList<ProductModel> productModels = new ArrayList<>();

		productModels.add(
				new ProductModel ("1","Producto 1",20.2f)
		);
		productModels.add(
				new ProductModel ("2","Producto 2",10)
		);
		productModels.add(
				new ProductModel ("3","Producto 3",12.2f)
		);
		productModels.add(
				new ProductModel ("4","Producto 4",3)
		);

		return productModels;
	}

}
